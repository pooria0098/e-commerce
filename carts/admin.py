from django.contrib import admin
from .models import Item, OrderItem, Order, Coupon, Refund, Address, Payment


def make_refund_accepted(modelAdmin, request, queryset):
    queryset.update(refund_requested=False, refund_granted=True)


make_refund_accepted.short_description = 'Update Order to refund granted'


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    model = Order
    list_display = [
        'user',
        'ordered',
        'being_delivered',
        'received',
        'refund_requested',
        'refund_granted',
        'billing_address',
        'shipping_address',
        'coupon',
        'payment'
    ]
    list_display_links = [
        'user',
        'ordered',
        'billing_address',
        'shipping_address',
        'coupon',
        'payment'
    ]
    list_filter = [
        'user',
        'ordered',
        'being_delivered',
        'received',
        'refund_requested',
        'refund_granted'
    ]
    search_fields = [
        'user__username',
        'ref_code__exact'
    ]
    actions = [make_refund_accepted]


@admin.register(Address)
class AddressAdmin(admin.ModelAdmin):
    list_display = [
        'user',
        'street_address',
        'apartment_address',
        'country',
        'address_type',
        'zip',
        'default'
    ]
    list_filter = [
        'address_type',
        'default',
        'country'
    ]
    search_fields = ['user__username', 'street_address', 'apartment_address', 'zip']


admin.site.register(Item)
admin.site.register(OrderItem)
admin.site.register(Coupon)
admin.site.register(Payment)
admin.site.register(Refund)